export interface Gauge {
  id: string;
  sensor: string;
  title: string;
  val: number;
  unit: string;
  lastUpdate: string;
  maxThreshArr: number[];
  minThreshArr: number[];
  upperLimit: number;
  lowerLimit: number;
  minLim: number;
  maxLim: number;
  minLow: number;
  maxLow: number;
  minHigh: number;
  maxHigh: number;
  plotBands: Array<object>;
  metric: boolean;
}