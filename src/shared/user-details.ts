export interface UserDetails {
  lastName: string;
  smsAddress: string;
  homePhone: string;
  title: string;
  firstName: string;
  emailAddress: string;
  mobilePhone: string;
  metric: boolean;
  countryCode: string;
  middleName: string;
  workPhone: string;
  map: boolean;
  name: string;
  description: string;
  language: string;
  lastConnection: number;
}