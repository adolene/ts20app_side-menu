export interface Alert {
  ackTimestamp: Date;
  alert_limit: number;
  alertType: string;
  inProgress: boolean;
  ackBy: string;
  ack: boolean;
  sourceProperty: string;
  source: string;
  message: string;
  alert_name: string;
  timestamp: Date;
  localeString: any;
}