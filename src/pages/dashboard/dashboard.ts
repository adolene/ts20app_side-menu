import { Component, OnInit, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Gauge } from '../../shared/gauge';
import { GaugeComponent } from '../../components/gauge/gauge';
import { DeviceProvider } from '../../providers/device/device';
import { UserProvider } from '../../providers/user/user';
import 'rxjs/add/operator/map';
import 'rxjs';



@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  gaugeInterface: Gauge;
  gauges: Gauge[] = [];

  constructor(  public navCtrl: NavController, 
                public navParams: NavParams, 
                public loadingCtrl: LoadingController, 
                private gauge: GaugeComponent, 
                private deviceService: DeviceProvider,
                private userService: UserProvider) { }

  ngOnInit() {
    this.userService.getUnitPref()
      .subscribe((units) => {
        this.userService.unit = units;
      })
  }

}
