import { Component, OnInit, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Device } from '../../shared/device';
import { DeviceProvider } from '../../providers/device/device';
import { UserProvider } from '../../providers/user/user';
import { Observable } from 'rxjs/Observable';
import { ProcessHttpProvider } from '../../providers/process-http/process-http';
//import 'rxjs/Rx';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';


@IonicPage({})
@Component({
  selector: 'page-device-status',
  templateUrl: 'device-status.html'
})
export class DeviceStatusPage implements OnInit {

  loader: any;

  uuids: string[];
  uuidsErrMsg: string;

  device: Device;
  devices: Device[] = [];
  devicesErrMsg: string;

  metric: boolean;
  unitString: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private processHttpService: ProcessHttpProvider,
    private deviceService: DeviceProvider,
    private userService: UserProvider,
    @Inject('BaseAPI') private BaseAPI) { 
      this.unitString = "°" + this.userService.unit;
     }

  ngOnInit() {
    //this.showLoader();

    this.deviceService.getUUIDs()
      .concatMap(res => Observable.from(res))
      .mergeMap((uuid) => this.deviceService.getDevice(uuid), null, 100)
      .filter((data: Device) => data.model != 'tsgw')
      .map((device: Device) => { 
        device.tc1 = this.convertAndParse(device.tc1);
        if (device.model === 'ts210' || device.model === 'ts220' || device.model === 'ts130') {
          device.tc2 = this.convertAndParse(device.tc2);
          if (device.model === 'ts130') {
            device.tc3 = this.convertAndParse(device.tc3);
            device.tc4 = this.convertAndParse(device.tc4);
            device.tc5 = this.convertAndParse(device.tc5);
            device.tc6 = this.convertAndParse(device.tc6);
            device.tc7 = this.convertAndParse(device.tc7);
            device.tc8 = this.convertAndParse(device.tc8);
            device.tc9 = this.convertAndParse(device.tc9);
            device.tc10 = this.convertAndParse(device.tc10);
          }
        }
        device.isOffline = this.detectOffline(device);
        device.parsedLastUpdate = this.parseLastUpdate(device.lastUpdate);
        return device;
      })
      .subscribe((device: Device) => { return this.devices.push(device) },
        errmess => this.devicesErrMsg = <any>errmess );
    //this.loader.dismiss();
  }

  convertAndParse(val: number) {
    if (this.userService.unit === 'F') {
      val = +(val * 9/5 + 32).toFixed(1);
    }
    else {
      val = +(val).toFixed(1);
    }
    return val;
  }

  detectOffline(device: Device): boolean {
    let isOffline: boolean;
    let now: number = Date.now();
    let lastUpdateNum: number = +(device.lastUpdate);
    let diff: number;

    switch (device.model) {
      case "ts210":
        diff = (now - lastUpdateNum) / 1000;
        if (diff > (device.freq * 124)) {
          isOffline = true;
        }
        else {
          isOffline = false;
        }
        break;
      case "ts120":
        diff = (now - lastUpdateNum) / 1000;
        if (diff > (device.freq * 124)) {
          isOffline = true;
        }
        else {
          isOffline = false;
        }
        break;
      case "ts220":
        diff = now - lastUpdateNum;
        if (diff > 2500009) {
          isOffline = true;
        }
        else {
          isOffline = false;
        }
        break;
      case "ts200":
        diff = now - lastUpdateNum;
        if (diff > 2500009) {
          isOffline = true;
        }
        else {
          isOffline = false;
        }
        break;
      case "ts110":
        diff = now - lastUpdateNum;
        if (diff > 300529) {
          isOffline = true;
        }
        else {
          isOffline = false;
        }
        break;
      case "ts100":
        diff = now - lastUpdateNum;
        if (diff > 300529) {
          isOffline = true;
        }
        else {
          isOffline = false;
        }
    }
    return isOffline;
  }

  parseLastUpdate(lastUpdate: Date): string {
    let parsed = new Date(lastUpdate).toLocaleString();
    return parsed;
  }

  showLoader() {
    this.loader = this.loadingCtrl.create({
      spinner: "crescent",
      duration: 2500,
      enableBackdropDismiss: true
    });

    this.loader.present()
  }

}
