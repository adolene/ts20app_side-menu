import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeviceStatusPage } from './device-status';

@NgModule({
  declarations: [
    DeviceStatusPage,
  ],
  imports: [
    IonicPageModule.forChild(DeviceStatusPage),
  ],
})
export class DeviceStatusPageModule {}
