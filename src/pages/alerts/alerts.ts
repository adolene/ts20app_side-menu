import { Component, OnInit, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { AlertDetailsPage } from '../alert-details/alert-details';
import { Alert } from '../../shared/alert';
import { Device } from '../../shared/device';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { DeviceProvider } from '../../providers/device/device';
import { ProcessHttpProvider } from '../../providers/process-http/process-http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/of';
import 'rxjs/Rx';


@Component({
  selector: 'page-alerts',
  templateUrl: 'alerts.html'
})
export class AlertsPage implements OnInit {
  test: any;
  
  alert: Alert;
  alerts: Alert[] = [];
  allAlerts: Array<Alert[]> = [];
  alertsErrMsg: string;

  activeAlerts: Alert[] = [];
  inProgressAlerts: Alert[] = [];
  resolvedAlerts: Alert[] = [];

  activeAlertsDisplay: Alert[] = [];
  inProgressAlertsDisplay: Alert[] = [];
  resolvedAlertsDisplay: Alert[] = [];

  uuids: string[];
  alertState: any;
  loading: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private processHttpService: ProcessHttpProvider,
    public alertsService: AlertsProvider,
    private deviceService: DeviceProvider,
    @Inject('BaseAPI') private BaseAPI) { 
      this.alertState = "active"; 
    }

  
  ngOnInit() {
    //this.showLoader();

    this.deviceService.getUUIDs()
      .concatMap(res => Observable.from(res))
      .mergeMap((uuid) => { return this.alertsService.getAlertSummary(uuid) })
      .flatMap(alertArray => Observable.from(alertArray))
      .subscribe((alertObj) => { return this.sortByState(alertObj) },
        errmess => this.alertsErrMsg = <any>errmess);
        //.subscribe((alerts: Alert[]) => { return this.allAlerts.push(alerts) },
    setTimeout(() => {
      if (this.activeAlerts.length > 0) {
        this.activeAlerts.sort( function(d1, d2) {
          if (d1.timestamp > d2.timestamp) {
            return -1;
          }
          else if (d2.timestamp > d1.timestamp) {
            return 1;
          }
          else {
            return 0;
          }
        });
        this.activeAlerts.map((alert: Alert) => {
          let d = new Date(alert.timestamp);
          alert.localeString = d.toLocaleString();
          return alert.localeString;
        });
      }

      if (this.inProgressAlerts.length > 0) {
        this.inProgressAlerts.sort( function(d1, d2) {
          if (d1.timestamp > d2.timestamp) {
            return -1;
          }
          else if (d2.timestamp > d1.timestamp) {
            return 1;
          }
          else {
            return 0;
          }
        });
        this.inProgressAlerts.map((alert: Alert) => {
          let d = new Date(alert.timestamp);
          alert.localeString = d.toLocaleString();
          return alert.localeString;
        });
      }

      if (this.resolvedAlerts.length > 0) {
        this.resolvedAlerts.sort( function(d1, d2) {
          if (d1.timestamp > d2.timestamp) {
            return -1;
          }
          else if (d2.timestamp > d1.timestamp) {
            return 1;
          }
          else {
            return 0;
          }
        });
        this.resolvedAlerts.map((alert: Alert) => {
          let d = new Date(alert.timestamp);
          alert.localeString = d.toLocaleString();
          return alert.localeString;
        });
      }

      if (this.activeAlerts.length > 0) {
        if (this.activeAlerts.length > 10) {
          if (this.alertsService.activeAlertsDisplay.length === 0) {
            for (let i = 0; i < 10; i++) {
              this.alertsService.activeAlertsDisplay.push(this.activeAlerts[i]);
            }
          }
          this.alertsService.activeDisplayIndex = this.alertsService.activeAlertsDisplay.length;
        }
        else {
          this.alertsService.activeAlertsDisplay = this.activeAlerts;
        }
      }
      if (this.inProgressAlerts.length > 0) {
        if (this.inProgressAlerts.length > 10) {
          if (this.alertsService.inProgressAlertsDisplay.length === 0) {
            for (let i = 0; i < 10; i++) {
              this.alertsService.inProgressAlertsDisplay.push(this.inProgressAlerts[i]);
            }
          }
          this.alertsService.inProgressDisplayIndex = this.alertsService.inProgressAlertsDisplay.length;
        }
        else {
          this.alertsService.inProgressAlertsDisplay = this.inProgressAlerts;
        }
      }
      if (this.resolvedAlerts.length > 0) {
        if (this.resolvedAlerts.length > 10) {
          if (this.alertsService.resolvedAlertsDisplay.length === 0) {
            for (let i = 0; i < 10; i++) {
              this.alertsService.resolvedAlertsDisplay.push(this.resolvedAlerts[i]);
            }
          }
          this.alertsService.resolvedDisplayIndex = this.alertsService.resolvedAlertsDisplay.length;
        }
        else {
          this.alertsService.resolvedAlertsDisplay = this.resolvedAlerts;
        }
      }
      //this.loading.dismiss();
      //this.presentSuccessToast();  // for testing appearance
      //this.presentFailToast();  // for testing appearance
    }, 1000);
  }

  sortByState(a: Alert): void {
    if (a.ack) {
      this.resolvedAlerts.push(a);
    }
    else if (a.inProgress && !a.ack) {
      this.inProgressAlerts.push(a);
    }
    else if (!a.ack) {
      this.activeAlerts.push(a);
    }
  }

  viewAlertDetails(alert: Alert) {
    this.navCtrl.push(AlertDetailsPage, { alertDetails: alert, alertState: this.alertState });
  }
  
  ackAlert(event, alert: Alert) {
    this.alertsService.acknowledgeAlert(alert)
      .subscribe(res => {
        if (res.result === "success") {
          this.presentSuccessToast();
          let index = this.alertsService.activeAlertsDisplay.indexOf(alert);
          if (index > -1) {
            this.alertsService.activeAlertsDisplay.splice(index, 1);
            this.activeAlerts.splice(index, 1);
          }
        }
      },
      error => {
        console.log(error);
        this.presentFailToast();
      });
  }

  presentSuccessToast() {
    const toast = this.toastCtrl.create({
      message: "Alert Acknowledged",
      duration: 1500,
      position: "top",
      cssClass: "ackSuccessToast",
      dismissOnPageChange: true
    });

    toast.present();
  }

  presentFailToast() {
    const toast = this.toastCtrl.create({
      message: "Something went wrong, please try again.",
      duration: 3000,
      position: "top",
      cssClass: "ackFailToast",
      dismissOnPageChange: true
    });

    toast.present();
  }

  updateDisplayIndex(alertState: string): void {
    switch (alertState) {
      case "active":

    }
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      switch (this.alertState) {
        case "active":
          let activeTotal = this.activeAlerts.length;
          let activeIndex = this.alertsService.activeDisplayIndex;
          let newActiveIndex = this.alertsService.activeDisplayIndex + 10;
          if (activeIndex < activeTotal) {
            for (let i = activeIndex; i < newActiveIndex; i++) {
              this.alertsService.activeAlertsDisplay.push(this.activeAlerts[i]);
            }
            this.alertsService.activeDisplayIndex = this.alertsService.activeAlertsDisplay.length;
          }
          break;

        case "inProgress":
          let progTotal = this.inProgressAlerts.length;
          let progIndex = this.alertsService.inProgressDisplayIndex;
          let newProgIndex = this.alertsService.inProgressDisplayIndex + 10;
          if (progIndex < progTotal) {
            for (let i = progIndex; i < newProgIndex; i++) {
              this.alertsService.inProgressAlertsDisplay.push(this.inProgressAlerts[i]);
            }
            this.alertsService.inProgressDisplayIndex = this.alertsService.inProgressAlertsDisplay.length;
          }
          break;

        case "resolved":
          let resTotal = this.resolvedAlerts.length;
          let resIndex = this.alertsService.resolvedDisplayIndex;
          let newResIndex = this.alertsService.resolvedDisplayIndex + 10;
          if (resIndex < resTotal) {
            for (let i = resIndex; i < newResIndex; i++) {
              this.alertsService.resolvedAlertsDisplay.push(this.resolvedAlerts[i]);
            }
            this.alertsService.resolvedDisplayIndex = this.alertsService.resolvedAlertsDisplay.length;
          }
      }
      infiniteScroll.complete();
    }, 1000);
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      spinner: "crescent"
    });
    this.loading.present();
  }

}
