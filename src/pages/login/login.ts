import { Component } from '@angular/core';
import { NavController, Platform, LoadingController, ToastController } from 'ionic-angular';
//import { NativeStorage } from '@ionic-native/native-storage';
import { AuthProvider } from '../../providers/auth/auth';
import { UserProvider } from '../../providers/user/user';
import { DashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  account: string;
  username: string;
  password: string;
  loader: any;
  emptyFields: boolean;

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              public authService: AuthProvider,
              public userService: UserProvider) { 
    //this.navCtrl = navCtrl;
    
  }

  login() {
    this.presentLoading();
    if (this.account && this.username && this.password) {
      //this.presentLoading();
      this.authService.login(this.account, this.username, this.password).then((result) => {
        if (result) {
          this.userService.getUnitPref()
            .subscribe(unit => {
              console.log("Unit Pref: ", unit);
              this.userService.unit = unit;
            });
          this.navCtrl.setRoot(DashboardPage);
          this.loader.dismiss();
        }
      }, (err) => {
          this.loader.dismiss();
          this.presentInvalidCredentialsToast();
          console.log(err);
      });
    }
    else {
      this.presentEmptyFieldsToast();
      console.log("empty fields detected");
    }
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      spinner: "crescent",
      content: "Authenticating",
      showBackdrop: true
    });

    this.loader.present();
  }

  presentEmptyFieldsToast() {
    const toast = this.toastCtrl.create({
      message: "Please do not leave any fields blank.",
      duration: 3000,
      position: "top",
      cssClass: "loginErrorToast"
    });

    toast.present();
  }

  presentInvalidCredentialsToast() {
    const toast = this.toastCtrl.create({
      message: "Invalid credentials - Please try again.",
      duration: 3500,
      position: "top",
      cssClass: 'loginErrorToast'
    });

    toast.present();
  }
  /*
  login(credentials) {

    (this.credentials.account).toLowerCase();
    (this.credentials.username).toLowerCase();

    this.authService.login(this.credentials).then((status) => {
      if (status == 200) {
        this.navCtrl.setRoot(DashboardPage);
      }
      else {
        console.log("Something went wrong")
      }
    });
  }
*/
}
