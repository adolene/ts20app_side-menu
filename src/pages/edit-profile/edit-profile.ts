import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserDetails } from '../../shared/user-details';
import { UserProvider } from '../../providers/user/user';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {

  userDetails: UserDetails;
  res;
  editErrMsg: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserProvider) { 
    this.userDetails = this.navParams.get('userDetails');
  }
  

  ngOnInit() {
    
  }

  saveProfileEdit(event, userDetails) {
    this.userService.editUserDetails(userDetails)
      .subscribe((response) => { return response },
        errmess => this.editErrMsg = <any>errmess);
  }

}
