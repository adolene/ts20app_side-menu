import { Component, OnInit, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserDetails } from '../../shared/user-details';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { UserProvider } from '../../providers/user/user';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage implements OnInit {

  userDetails: UserDetails;
  userDetailsErrMsg: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private userService: UserProvider,
    private authService: AuthProvider,
    @Inject('BaseAPI') private BaseAPI) { }

  ngOnInit() {
    this.userService.getUserDetails()
      .subscribe(userDetails => this.userDetails = userDetails,
        errmess => this.userDetailsErrMsg = <any>errmess );
  }

  editProfile(event, userDetails: UserDetails) {
    this.navCtrl.push(EditProfilePage, { userDetails: userDetails });
  }

}
