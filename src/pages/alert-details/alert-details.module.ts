import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AlertDetailsPage } from './alert-details';

@NgModule({
  declarations: [
    AlertDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(AlertDetailsPage),
  ],
})
export class AlertDetailsPageModule {}
