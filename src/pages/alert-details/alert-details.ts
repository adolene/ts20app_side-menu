import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Alert } from '../../shared/alert';
import { Device } from '../../shared/device';
import { AlertsProvider } from '../../providers/alerts/alerts';
import { DeviceProvider } from '../../providers/device/device';
import { UserProvider } from '../../providers/user/user';


@IonicPage()
@Component({
  selector: 'page-alert-details',
  templateUrl: 'alert-details.html',
})
export class AlertDetailsPage {

  alert: Alert;
  alertState: string;
  device: Device;
  parentDeviceName: string;
  unit: string;
  disableAck: boolean;

  message: string;
  parsedAlertType: string;
  parsedTimestamp: string;
  resolution: string;
  threshold: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertsService: AlertsProvider,
    private deviceService: DeviceProvider,
    private userService: UserProvider) {
      this.alert = navParams.get('alertDetails');
      this.alertState = navParams.get('alertState');
      this.disableAck = false;
      this.parentDeviceName = "";
  }

  ngOnInit() {
    this.deviceService.getDevice(this.alert.source)
      .subscribe(device => {
        this.device = device;
        this.parentDeviceName = device.MyName;
        console.log(this.device);
      });

    if (this.alert.sourceProperty === "humidity") {
      this.unit = "%";
    }
    else if (this.alert.sourceProperty.indexOf("tc") > -1) {
      this.unit = "°" + this.userService.unit;
    }

    console.log(this.alert);
    this.threshold = this.convertAndParse(this.alert.alert_limit);
    this.parsedTimestamp = this.parseTimestamp(this.alert.timestamp);
    this.generateMessage(this.alert.alertType, this.alert.source, this.alert.sourceProperty);
    if (this.alertState === "resolved") {
      this.generateResolution(this.alert.message);
    }
  }

  convertAndParse(val: number): number {
    if (this.userService.unit === 'F') {
      val = +(val * 9/5 + 32).toFixed(1);
    }
    else {
      val = +(val).toFixed(1);
    }
    return val;
  }

  parseTimestamp(timestamp: Date): string {
    //let d = new Date(timestamp)
    let parsed = new Date(timestamp).toLocaleString();
    return parsed;
  }

  generateMessage(alertType: string, source: string, sourceProperty: string) {

    let subj: string, obj: string, adj: string; 

    if (sourceProperty.indexOf("tc") > -1) {
      obj = "temperature";
      subj = "Sensor " + sourceProperty;
    }
    else if (sourceProperty === "humidity") {
      obj = "humidity";
      subj = "The humidity sensor";
    }
    else if (sourceProperty === "light") {
      obj = "lux";
      subj = "The light sensor";
    }
    else if (sourceProperty === "kpa") {
      obj = "pressure";
      subj = "The pressure sensor";
    }

    if (alertType === "Below") {
      adj = "less than";
    }
    else if (alertType === "Above") {
      adj = "greater than";
    }
    this.message = subj + " recorded a " + obj + " value " + adj;
  }

  generateResolution(resolution: string): void {
    if (resolution === "Deleted Alert") {
      this.resolution = "The alert was manually deleted";
    }
  }
  
  ackAlert(event, alert: Alert) {
    this.alertsService.acknowledgeAlert(alert)
      .subscribe(res => {
        if (res.result === "success") {
          let index = this.alertsService.activeAlertsDisplay.indexOf(alert);
          if (index > -1) {
            this.alertsService.activeAlertsDisplay.splice(index, 1);
            //this.alertsPage.activeAlerts.splice(index, 1);
            this.disableAck = true;
          }
        }
      }, 
        error => {
          console.log(error);
      });
  }

}