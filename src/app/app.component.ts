import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { NativeStorage } from '@ionic-native/native-storage';
//import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage';  // uncomment when running with Cordova

import { AuthProvider } from '../providers/auth/auth';
import { UserProvider } from '../providers/user/user';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { DeviceStatusPage } from '../pages/device-status/device-status';
import { AlertsPage } from '../pages/alerts/alerts';
import { UserPage } from '../pages/user/user';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  loader: any;

  pages: Array<{title: string, icon: string, component: any}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen, 
    public loadingCtrl: LoadingController) {
      
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', icon: 'md-speedometer', component: DashboardPage },
      { title: 'Device Status', icon: 'list', component: DeviceStatusPage },
      { title: 'Alerts', icon: 'ios-notifications-outline', component: AlertsPage },
      { title: 'Profile', icon: 'person', component: UserPage }
    ];

  }

  // Dev/Debug only with ionic serve (skips log in and auto logs into chobfactory as admin)
   /*
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.presentLoading();

      let credentials = {
        account: "chobfactory",
        username: "admin",
        password: "123456"
      };

      this.authService.login(credentials.account, credentials.username, credentials.password)
        .then((result) => {
          this.checkUnits();
          this.nav.setRoot(DashboardPage);
          this.loader.dismiss();
        },
        (err) => {
          this.nav.setRoot(LoginPage);
          this.loader.dismiss();
        });
    });
  }
   */

  // Login Required version
  // /*
  initializeApp() {
    this.platform.ready().then(() => {
      
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  // */

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      spinner: "crescent"
    });
    this.loader.present();
  }

  logout() {
    //incorporate modal - logout confirmation
    this.nav.setRoot(LoginPage);
  }
}
