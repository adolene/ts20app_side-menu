import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
//import { Fingerprint AIO } from '@ionic-native/fingerprint-aio';
import { NativeStorage } from '@ionic-native/native-storage';
import { SecureStorage } from '@ionic-native/secure-storage';

import { ChartModule } from 'angular2-highcharts/index';
declare var require: any;
const SolidGauge = require('highcharts/modules/solid-gauge');
import * as Highcharts from 'highcharts';
SolidGauge(Highcharts);
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { DeviceStatusPage } from '../pages/device-status/device-status';
import { AlertsPage } from '../pages/alerts/alerts';
import { AlertDetailsPage } from '../pages/alert-details/alert-details';
import { UserPage } from '../pages/user/user';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { GaugeComponent } from '../components/gauge/gauge';

import { baseAPI } from '../shared/base-api';
import { Alert } from '../shared/alert';
import { UserDetails } from '../shared/user-details';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';
import { ProcessHttpProvider } from '../providers/process-http/process-http';
import { UserProvider } from '../providers/user/user';
import { DeviceProvider } from '../providers/device/device';
import { AlertsProvider } from '../providers/alerts/alerts';

declare var require: any;
export function highchartsFactory() {
  return require('highcharts');
}

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    DashboardPage,
    DeviceStatusPage,
    AlertsPage,
    AlertDetailsPage,
    UserPage,
    EditProfilePage,
    GaugeComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpModule,
    ChartModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    DashboardPage,
    DeviceStatusPage,
    AlertsPage,
    AlertDetailsPage,
    UserPage,
    EditProfilePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Storage,
    NativeStorage,
    SecureStorage,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: 'BaseAPI', useValue: baseAPI },
    { provide: HighchartsStatic, useFactory: highchartsFactory },
    AuthProvider,
    ProcessHttpProvider,
    UserProvider,
    DeviceProvider,
    AlertsProvider,
    GaugeComponent
  ]
})
export class AppModule {}
