import { Injectable } from '@angular/core';
import { Device } from '../../shared/device';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { baseAPI } from '../../shared/base-api';
import { ProcessHttpProvider } from '../process-http/process-http';
import { AuthProvider } from '../../providers/auth/auth';
import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';


@Injectable()
export class DeviceProvider {

  constructor(public http: Http,
    private processHttpService: ProcessHttpProvider,
    private authService: AuthProvider,
    private secureStorage: SecureStorage) { }

  getUUIDs(): Observable<string[]> {
    let headers = new Headers();
    headers.append('Accept', 'Application/json');

    let body = { };
    return this.http.post(baseAPI + 'ThingShapes/TSGenericShape/Services/GetImplementingThings?appKey=' + this.authService.appKey, body, {headers:headers})
      .map(res => { return this.processHttpService
        .extractUUIDs(res); })
      .catch(error => { return this.processHttpService
        .handleError(error); });
  }

  getDeviceWithData(uuid: string) {
    let headers = new Headers();
    headers.append('Accept', 'Application/json');

    return this.http.get(baseAPI + 'Things/' + uuid + '/Properties?appKey=' + this.authService.appKey, {headers:headers})
      .map(res => { return this.processHttpService
        .extractDataRowsZero(res); })
      .catch(error => { return this.processHttpService
        .handleError(error); });
  }

  getDevice(uuid: string): Observable<Device> {
    let headers = new Headers();
    headers.append('Accept', 'Application/json');

    return this.http.get(baseAPI + 'Things/' + uuid + '/Properties?appKey=' + this.authService.appKey, {headers:headers})
      .map(res => { return this.processHttpService
        .extractDataRowsZero(res); })
      .catch(error => { return this.processHttpService
        .handleError(error); });
      
  }
    
  
}
