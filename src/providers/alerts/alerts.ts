import { Injectable } from '@angular/core';
import { Alert } from '../../shared/alert';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { baseAPI } from '../../shared/base-api';
import { ProcessHttpProvider } from '../process-http/process-http';
import { AuthProvider } from '../auth/auth';
import { DeviceProvider } from '../device/device';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';


@Injectable()
export class AlertsProvider {

  activeAlerts: Alert[] = [];
  inProgressAlerts: Alert[] = [];
  resolvedAlerts: Alert[] = [];

  activeAlertsDisplay: Alert[] = [];
  inProgressAlertsDisplay: Alert[] = [];
  resolvedAlertsDisplay: Alert[] = [];

  activeDisplayIndex: number;
  inProgressDisplayIndex: number;
  resolvedDisplayIndex: number;

  constructor(public http: Http,
    private processHttpService: ProcessHttpProvider,
    private authService: AuthProvider,
    private deviceService: DeviceProvider) { }


    getAlertSummary(uuid: string): Observable<Alert[]> {
      let headers = new Headers();
      headers.append('Accept', 'Application/json');
      let body = { device_uuid: uuid }

      return this.http.post( baseAPI + 'Things/' + this.authService.account + "Handler/Services/getAlertSummary?appKey=" + this.authService.appKey, body, {headers:headers} )
        .map(res => { return this.processHttpService
          .extractAlertSummary(res); })
        .catch(error => { return this.processHttpService
          .handleError(error); });
    }


    getAlertDefinitions(uuid: string, property: string): Observable<any> {
      let headers = new Headers();
      headers.append('Accept', 'Application/json');
      let body = { property: property }

      return this.http.post( baseAPI + 'Things/' + uuid + '/Services/GetAlertDefinitions?appKey=' + this.authService.appKey, body, {headers:headers} )
        .map(res => { return this.processHttpService
          .extractDataRowsZero(res); })
        .catch(error => { return this.processHttpService
          .handleError(error); });
    }

    acknowledgeAlert(alert: Alert): Observable<any> {
      let headers = new Headers();
      headers.append('Accept', 'Application/json');
      
      let body = {
        alert_name: alert.alert_name,
        message: alert.message,
        source: alert.source,
        sourceProperty: alert.sourceProperty,
        timestamp: alert.timestamp,
        username: this.authService.combinedCredentials
      }

      return this.http.post( baseAPI + 'Things/' + this.authService.account + 'Handler/Services/ackAlerttry?appKey=' + this.authService.appKey, body, {headers:headers} )
        .map(res => { return this.processHttpService
          .extractData(res); })
        .catch(error => { return this.processHttpService
          .handleError(error); });
    }

}
