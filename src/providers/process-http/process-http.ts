import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/observable/throw';


@Injectable()
export class ProcessHttpProvider {

  constructor(public http: Http) { }

  public extractLogIn(res: Response) {
    let body = res.json();
    return body || { };
  }

  public extractUnitPref(res: Response) {
    let body = res.json().rows[0];
    console.log(body);
    let unit: string;
    if (body.metric) {
      unit = "C";
    }
    else {
      unit = "F";
    }
    return unit;
  }

  public extractData(res: Response) {
    let body = res.json();
    console.log(res.json());
    return body || { };
  }

  public extractDataRows(res: Response) {
    let body = res.json().rows;
    console.log(body);
    return body || { };
  }

  public extractDataRowsZero(res: Response) {
    let body = res.json().rows[0];
    //console.log(body);
    return body || { };
  }

  public extractUUIDs(res: Response) {
    let body = [];
    let uuids = res.json().rows;
    for (let uuid of uuids) {
      body.push(uuid.name);
    }
    console.log(body);
    return body || [ ];
  }

  public extractAlertSummary(res: Response) {
    let summaries = res.json().rows;
    return summaries || [ ];
  }

  public handleError(error: Response | any) {

    let errMsg: string;

    if (error instanceof Response) {

      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);

      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    }
    else {

      errMsg = error.message ? error.message : error.toString();
    }

    console.error(errMsg);

    return Observable.throw(errMsg);
  }

}
