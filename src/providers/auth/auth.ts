import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { baseAPI } from '../../shared/base-api';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class AuthProvider {
  loading: any;
  appKey: string;
  account: string;
  username: string;
  combinedCredentials: string;

  constructor(public http: Http, 
              public loadingCtrl: LoadingController) { }
/*
  checkAuthStatus() {
    return new Promise((resolve, reject) => {

      this.secureStorage.create('ssAppKey')
        .then((storage: SecureStorageObject) => {
          storage.get('appKey')
            .then(
              storedKey => { 
                this.appKey = storedKey;
                resolve(storedKey);
              },
              error => {
                console.log(error);
                reject();
              }
            );
        });     
    });
  }
*/
  login(account: string, username: string, password: string) {
    //this.showLoader();
    account = account.toLowerCase();
    username = username.toLowerCase();
    
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Accept', 'Application/json');

      this.http.get( 'https://telesense.net/Thingworx/ApplicationKeys/' + account + username + 'key/?userid=' + account + username + '&password=' + password, {headers:headers} )
        .subscribe(res => {

          let data = res.json();

          if (res.status === 200) {
            this.appKey = data.keyId;
            this.account = account;
            this.username = username;
            this.combinedCredentials = data.userNameReference;
          }
          //this.loading.dismiss();
          resolve(data);
        },
          (err) => {
            reject(err);
      });
    });
  }

  logout() {
    this.appKey = null;
    this.username = null;
    this.account = null;
    this.combinedCredentials = null;
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content:'Authenticating',
      spinner: 'crescent'
    });

    this.loading.present();
  }

}
