import { Injectable } from '@angular/core';
import { UserDetails } from '../../shared/user-details';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { baseAPI } from '../../shared/base-api';
import { AuthProvider } from '../auth/auth';
import { ProcessHttpProvider } from '../process-http/process-http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';


@Injectable()
export class UserProvider {

  unit: string;

  constructor(public http: Http,
    public authService: AuthProvider,
    private processHttpService: ProcessHttpProvider) { }
  
  getUserDetails(): Observable<UserDetails> {
    let headers = new Headers();
    headers.append('Accept', 'Application/json');
    return this.http.get(baseAPI + 'Users/' + this.authService.combinedCredentials + '/Properties?appKey=' + this.authService.appKey, {headers:headers} )
      .map(res => { return this.processHttpService
        .extractDataRowsZero(res); })
      .catch(error => {return this.processHttpService
        .handleError(error); });
  }

  getUnitPref(): Observable<string> {
    let headers = new Headers();
    headers.append('Accept', 'Application/json');
    return this.http.get( baseAPI + 'Users/' + this.authService.combinedCredentials + '/Properties/metric?appKey=' + this.authService.appKey, {headers:headers} )
      .map(res => { return this.processHttpService
        .extractUnitPref(res); })
      .catch(error => { return this.processHttpService
        .handleError(error); });
  }

  editUserDetails(userDetails: UserDetails): Observable<any> {
    let headers = new Headers();
    headers.append('Accept', 'Application/json');
    let body = {
      emailAddress: userDetails.emailAddress,
      firstName: userDetails.firstName,
      lastName: userDetails.lastName,
      title: userDetails.title,
      mobilePhone: userDetails.mobilePhone,
      countryCode: userDetails.countryCode,
      username:this.authService.username
    };
/*
    let body = {
      emailAddress: userDetails.emailAddress,
      firstName: userDetails.firstName,
      lastName: userDetails.lastName,
      title: userDetails.title,
      mobilePhone: userDetails.mobilePhone,
      metric: userDetails.metric,
      countryCode: userDetails.countryCode,
      username: userDetails.name,
      map: userDetails.map,
      workPhone: userDetails.workPhone,
      smsAddress: userDetails.smsAddress,
      homePhone: userDetails.homePhone,
      middleName: userDetails.middleName
    }
*/
    return this.http.post( baseAPI + 'Things/UserHandler/Services/UpdateUserExt?appKey=' + this.authService.appKey, body, {headers:headers} )
      .map(res => { return this.processHttpService
        .extractData(res); })
      .catch(error => { return this.processHttpService
        .handleError(error); });
  }

}
