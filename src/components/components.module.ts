import { NgModule } from '@angular/core';
import { GaugeComponent } from './gauge/gauge';
@NgModule({
	declarations: [GaugeComponent],
	imports: [],
	exports: [GaugeComponent]
})
export class ComponentsModule {}
