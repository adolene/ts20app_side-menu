import { Component, OnInit, Inject } from '@angular/core';
import { ChartModule } from 'angular2-highcharts/index';
import { Gauge } from '../../shared/gauge';

declare var require: any;
var Highcharts = require('highcharts/highcharts');
var HighchartsMore = require('highcharts/highcharts-more');
HighchartsMore(Highcharts);

@Component({
  selector: 'gauge',
  templateUrl: 'gauge.html'
})
export class GaugeComponent {

  chart: any;
  options: any;
  gauge: Gauge;
  gauges: Gauge[];

  saveInstance(chartInstance) {
    this.chart = chartInstance
  }

  constructor(public highcharts: ChartModule) { }

  ngOnInit() {

    this.options = {
      chart: {
        type: 'solidgauge'
      },
      title: 'Test',
      credits: {
        enabled: false
      },
      pane: {
        center: ['50%', '50%'],
        size: '100%',
        startAngle: -90,
        endAngle: 90,
        background: {
          innerRadius: '60%',
          outerRadius: '100%',
          shape: 'arc'
        }
      },
      tooltip: {
        enabled: false
      },
      yAxis: {
        stops: [
          [0.1, '#55BF3B'],
          [0.5, '#DDDF0D'],
          [0.9, '#DF5353']
        ],
        lineWidth: 0,
        min: 0,
        max: 120,
        minorTickInterval: null,
        tickAmount: 2,
        title: {
          text: 'wut',
          y: -70
        },
        labels: {
          y: 16
        }
      },
      plotOptions: {
        solidgauge: {
          dataLabels: {
            y: 5,
            borderWidth: 0,
            useHTML: true
          }
        }
      },
      series: [{
        name: 'hi',
        data: [78],
        dataLabels: {
          enabled: true,
          format: "{y}"
        }
      }]
    };

  } 
}
