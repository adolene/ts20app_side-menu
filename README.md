TeleSense 2.0 Mobile Application (Side-menu Layout - See 'ts20App' repo for tabs layout)

Ionic 2 | Angular 2.x

Author: Eric Tingleff | eric@telesense.com

------------------------

TO DO:

	High-Level/Cross-App:

		MVP:

			DONE? 1* - Invalid login behavior/logic
				- toast/indicator with error message
				- empty fields
				- invalid credentials
			2 - Pick Logout button location (side menu or bottom of profile page?)
			1 - SplashScreen

		Ideas/Future Implementation:

			- Security (to prevent forcing the user to login every time the app opens)
				- On login, grant JWT (stored in device storage) valid for a couple of weeks - session renewed if used before expiration

	Dashboard:

		MVP:

			1 - Pick a layout
				- Angular Gauge or Solid Gauge or No Gauges (something else)
				- Bigger and more detailed representations or smaller/compact
			1 - Implement chosen layout

		Ideas/Future Implementation:

			-

	Device Status:

		MVP:

			3	- Signal strength representation if applicable
			2	- Batt level indicator if applicable
			2	- Plug icon indicator if applicable
			2	- Location if applicable

		Ideas/Future Implementation:

			- Tap device for detailed view, 'Device Settings' modeled after TS Web 2.0, basic report gen, or stat graphs
			- Toolbar buttons for list view vs. tile or card view
			- Order by button/tool
				- Name, last update, Online/Offline

	Alerts:

		MVP:

			1 - Testing/Debug
			2 - Refresher

		Ideas/Future Implementation:

			- Badges/local notifications
			-	item-slider button 'Details' (does the same thing as current behavior triggered by clicking an alert item)
			- Order by button/tool
				- Name, time triggered, time acknowledged
			- Filter button/tool
				- Name, upper/lower threshold triggered, acknowledged by, device location, sensor type (temp, humidity etc.)


	Profile:

		MVP:

			1 - Implement edit profile info/preferences functionality

		Ideas/Future Implementation:




